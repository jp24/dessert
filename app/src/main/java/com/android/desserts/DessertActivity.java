package com.android.desserts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.desserts.dbhelper.DessertDatabaseHelper;

public class DessertActivity extends AppCompatActivity {

    public static final String EXTRA_DESSERTID = "dessertId";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dessert);

        int desssertId = (Integer) getIntent().getExtras().get(EXTRA_DESSERTID);

        SQLiteOpenHelper dessertDatabaseHelper = new DessertDatabaseHelper(this);
        try {
            SQLiteDatabase db = dessertDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.query("DESSERT",
                    new String[]{"NAME", "DESCRIPTION",
                            "IMAGE_RESOURCE_ID", "FAVORITE"},
                    "_id = ?",
                    new String[]{Integer.toString(desssertId)},
                    null, null, null);

            if (cursor.moveToFirst()) {

                String nameText = cursor.getString(0);
                String descriptionText = cursor.getString(1);
                int photoId = cursor.getInt(2);
                boolean isFavorite = (cursor.getInt(3) == 1);

                TextView name = (TextView) findViewById(R.id.name);
                name.setText(nameText);

                TextView description = (TextView) findViewById(R.id.description);
                description.setText(descriptionText);

                ImageView photo = (ImageView) findViewById(R.id.photo);
                photo.setImageResource(photoId);
                photo.setContentDescription(nameText);

                CheckBox favorite = (CheckBox) findViewById(R.id.favorite);
                favorite.setChecked(isFavorite);
            }
            cursor.close();
            db.close();

        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(this, "Baza danych jest niedostępna",
                    Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void onFavoriteClicked(View view) {
        int desssertId = (Integer) getIntent().getExtras().get(EXTRA_DESSERTID);
        new UpdateDessertTask().execute(desssertId);
    }

    private class UpdateDessertTask extends AsyncTask<Integer, Void, Boolean> {
        private ContentValues dessertValues;

        protected void onPreExecute() {
            CheckBox favorite = (CheckBox) findViewById(R.id.favorite);
            dessertValues = new ContentValues();
            dessertValues.put("FAVORITE", favorite.isChecked());
        }

        protected Boolean doInBackground(Integer... drinks) {
            int drinkId = drinks[0];
            SQLiteOpenHelper dessertDatabaseHelper =
                    new DessertDatabaseHelper(DessertActivity.this);
            try {
                SQLiteDatabase db = dessertDatabaseHelper.getWritableDatabase();
                db.update("DESSERT", dessertValues,
                        "_id = ?", new String[]{Integer.toString(drinkId)});
                db.close();
                return true;
            } catch (SQLiteException e) {
                return false;
            }
        }

        protected void onPostExecute(Boolean success) {
            if (!success) {
                Toast toast = Toast.makeText(DessertActivity.this,
                        "Baza danych jest niedostępna", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

}
