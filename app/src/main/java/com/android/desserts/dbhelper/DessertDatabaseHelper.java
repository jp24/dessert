package com.android.desserts.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.android.desserts.R;

public class DessertDatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "desserts";
    private static final int DB_VERSION = 1;

    public DessertDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db, 0, DB_VERSION);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db, oldVersion, newVersion);
    }


    private static void insertDessert(SQLiteDatabase db, String name, String description, int resourceId){
        ContentValues dessertValues = new ContentValues();
        dessertValues.put("NAME", name);
        dessertValues.put("DESCRIPTION", description);
        dessertValues.put("IMAGE_RESOURCE_ID", resourceId);
        db.insert("DESSERT", null, dessertValues);
    }

    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 1) {
            db.execSQL("CREATE TABLE DESSERT (_id INTEGER PRIMARY KEY AUTOINCREMENT, " + "NAME TEXT, "
                    + "DESCRIPTION TEXT, " + "IMAGE_RESOURCE_ID INTEGER);");
            insertDessert(db, "Ptyś", "Pyszne ciastka z masą kremową.",
                    R.drawable.ptys);
            insertDessert(db, "Ciastka z kremem", "Smakowite ciastka kremowe z dodatkiem truskawek.",
                    R.drawable.cream_cakes);
            insertDessert(db, "Tiramisu", "Pyszny biszkopt nasączony mocną kawą z dodatkiem kremu" +
                            "oraz czekolady.",
                    R.drawable.tiramisu);
        }
        if (oldVersion < 2) {
            db.execSQL("ALTER TABLE DESSERT ADD COLUMN FAVORITE NUMERIC;");// Kod dodający nową kolumnę tabeli
        }
    }
}
